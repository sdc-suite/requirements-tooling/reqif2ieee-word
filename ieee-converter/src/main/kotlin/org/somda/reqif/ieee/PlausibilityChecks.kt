package org.somda.reqif.ieee

import org.somda.reqif.ieee.rendering.simpleString
import org.somda.reqif.ieee.spectype.obj.*
import org.somda.reqif.tree.ReqIfNode
import org.somda.reqif.tree.ReqIfTree

class PlausibilityChecks(val tree: ReqIfTree) {
    private val definitions = tree.let { reqIfTree ->
        reqIfTree.groups[DefinitionType::class].map {
            when (it.theObject is DefinitionType) {
                true -> it.theObject.value.term
                false -> throw Exception("Definition type is incomplete")
            }
        }
    }

    private val abbreviations = tree.let { reqIfTree ->
        reqIfTree.groups[AbbreviationType::class].map {
            when (it.theObject is AbbreviationType) {
                true -> it.theObject.value.name
                false -> throw Exception("Abbreviation type is incomplete")
            }
        }
    }

    fun runPlausibilityChecks() {
        System.err.println("\nPlausibility checks start")
        System.err.println("=========================")
        System.err.println("----")
        tree.rootNodes.parallelStream().forEach(::runPlausibilityChecks)
        System.err.println("============================")
        System.err.println("Plausibility checks finished\n")
    }

    private fun runPlausibilityChecks(node: ReqIfNode) {
        findPotentialInconsistencies(node)
        node.children.parallelStream().forEach(::runPlausibilityChecks)
    }

    private fun findPotentialInconsistencies(node: ReqIfNode) {
        val text = when (node.theObject) {
            is RequirementType -> node.theObject.value.text.simpleString()
            is ParagraphType -> node.theObject.value.text.simpleString()
            is ListItemType -> node.theObject.value.text.simpleString()
            is NoteType -> node.theObject.value.text.simpleString()
            is PlainTextType -> node.theObject.value
            else -> ""
        }

        findPotentialNonCoveredDefinitions(text)

        doPluralCheck(definitions, text, "definition")
        doMixedCaseCheck(definitions, text, "definition")
        doMixedCaseCheck(abbreviations, text, "abbreviation")
    }

    private fun findPotentialNonCoveredDefinitions(text: String) {
        // todo ignored for the moment
//        val cleanWord: (String) -> String = {
//            var lastChar = ' '
//            if (it.length > 1) {
//                lastChar = it[it.length - 1]
//            }
//
//            when (it.length > 1 && (lastChar.toLowerCase() == 's' || lastChar == '.' || lastChar == ',')) {
//                true -> {
//                    var offset = 1
//                    if (it.length > 2 && it[it.length - 2] == '\'') {
//                        offset = 2
//                    }
//                    it.substring(0, it.length - offset).trim()
//                }
//                false -> it.trim()
//            }
//        }
//
//        var fullWord = ""
//        val potentiallyUncoveredDefs = HashSet<String>()
//        for (word in text.split("\\s".toRegex()).map(cleanWord).toList()) {
//            if (word == word.toUpperCase()) {
//                fullWord += " $word"
//            } else {
//                fullWord = fullWord.trim()
//                if (!definitions.contains(fullWord) && !abbreviations.contains(fullWord)) {
//                    potentiallyUncoveredDefs.add(fullWord)
//                    fullWord = ""
//                }
//            }
//        }
//        if (fullWord.isNotEmpty()) {
//            potentiallyUncoveredDefs.add(fullWord)
//        }
//        potentiallyUncoveredDefs
//            .filter { it.isNotBlank() }
//            .forEach { println("Found potential uncovered definition: $it") }
    }

    private fun doMixedCaseCheck(needles: List<String>, haystack: String, label: String) {
        needles.stream().forEach {
            listOf(it, "${it}s").parallelStream().forEach { needle ->
                var index = needle.length * -1
                do {
                    index = haystack.indexOf(" $needle ", index + needle.length, true)
                    if (index == -1) {
                        break
                    } else {
                        val substr = haystack.substring(index, index + needle.length + 2).trim()
                        if (substr != needle) {
                            System.err.println(
                                "Found potential $label reference with lower case letters: $needle != $substr"
                            )
                            System.err.println("----")
                        }
                    }
                } while (true)
            }
        }
    }

    private fun doPluralCheck(needles: List<String>, haystack: String, label: String) {

        needles.stream().forEach {
            "${it.substring(0, it.length - 1)}ies".also { needle ->
                var index = needle.length * -1
                do {
                    index = haystack.indexOf(" $needle ", index + needle.length, true)
                    if (index == -1) {
                        break
                    } else {
                        val substr = haystack.substring(index, index + needle.length + 2).trim()
                        System.err.println(
                            "Found potential $label reference named '$substr' as plural that ends on 'ies'. " +
                                    "Should be 's' only: ${it}s"
                        )
                        System.err.println("----")
                    }
                } while (true)
            }
        }
    }
}