package org.somda.reqif.ieee

import org.somda.reqif.ieee.rendering.findFirstChild
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.w3c.dom.Node
import org.xml.sax.InputSource
import java.io.InputStream
import javax.xml.parsers.DocumentBuilderFactory

class ReqIfDocument(input: InputStream, specificationId: String = "") : Iterable<ObjectType> {
    private val docBuilder = DocumentBuilderFactory.newInstance().also { it.isNamespaceAware = true }
    private val xmlAccess: XmlAccess = XmlAccess(docBuilder.newDocumentBuilder().parse(InputSource(input)))

    private val specification: Node

    init {
        val specs = xmlAccess.specifications()
        specification = when (specs.length) {
            0 -> throw Exception("No specifications found")
            1 -> specs.item(0)
            else -> xmlAccess.findNodeById(specificationId, specs)
                ?: throw Exception("No specification with ID $specificationId found")
        }
    }

    override fun iterator(): Iterator<ObjectType> = DocumentIterator()

    private fun toObjectType(depth: Int, index: Int, node: Node) =
        ObjectTypeMapper(xmlAccess).toObjectType(depth, index, node)

    private inner class DocumentIterator : Iterator<ObjectType> {
        var nextNode: Node? = specification
            .findFirstChild(ReqIfQNames.CHILDREN)
            ?.findFirstChild(ReqIfQNames.SPEC_HIERARCHY)
        var nextDepth: Int = 0
        val indexMap = mutableMapOf<Int, Int>()

        override fun hasNext() = nextNode != null

        override fun next(): ObjectType {
            val currentNode = nextNode ?: throw Exception(
                "No next iterator element found. Do not call next() if hasNext() returns false."
            )
            val currentDepth = nextDepth
            val currentIndex = ((indexMap[currentDepth] ?: -1) + 1).also {
                indexMap[currentDepth] = it
            }

            val firstChild = currentNode
                .findFirstChild(ReqIfQNames.CHILDREN)
                ?.findFirstChild(ReqIfQNames.SPEC_HIERARCHY)
            if (firstChild != null) {
                nextNode = firstChild
                nextDepth++
            } else {
                nextNode = nextElementSibling(currentNode)
                var parentNode = currentNode
                while (nextNode == null) {
                    parentNode = parentNode.parentNode?.parentNode ?: break
                    nextNode = nextElementSibling(parentNode)
                    nextDepth--
                }
            }

            if (nextDepth < currentDepth) {
                var depthToReset = nextDepth + 1
                do {
                    indexMap[depthToReset] = -1
                } while (depthToReset++ != currentDepth)
            }
            return toObjectType(currentDepth, currentIndex, currentNode)
        }
    }

    private fun nextElementSibling(node: Node): Node? {
        var nextSibling = node.nextSibling
        while (nextSibling != null && nextSibling.localName == null) {
            nextSibling = nextSibling.nextSibling
        }

        return nextSibling
    }
}