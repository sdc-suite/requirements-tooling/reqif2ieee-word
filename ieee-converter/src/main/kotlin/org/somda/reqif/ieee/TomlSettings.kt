package org.somda.reqif.ieee

enum class TomlSettings(val tomlPath: String) {
    POCSPEC_DOCUMENT_TITLE("pocspec.document_title")
}