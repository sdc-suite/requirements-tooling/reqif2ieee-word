package org.somda.reqif.ieee.datatype

import org.somda.reqif.ieee.ReqIfQNames
import org.somda.reqif.ieee.rendering.simpleString
import org.w3c.dom.*

class FormattedString(node: Node) : Node by node {
    val maxStrLenWithoutDots = 40
    override fun toString(): String {

        val str = this.simpleString()
        if (str.length <= maxStrLenWithoutDots) {
            return str
        }
        return "${str.substring(0, maxStrLenWithoutDots).trim()} [...]"
    }
}

fun defaultFormattedString(): FormattedString = FormattedString(DefaultFormattedString())

fun simpleFormattedString(text: String): FormattedString =
    FormattedString(FormattedStringContainer(DefaultFormattedString(text)))

private class DefaultFormattedString(private val text: String = "") : Node {
    // getter methods
    override fun getNodeName(): String? {
        return null
    }

    override fun getNodeValue(): String? = text

    override fun getNodeType(): Short {
        return Node.TEXT_NODE
    }

    override fun getParentNode(): Node? {
        return null
    }

    override fun getChildNodes(): NodeList? = ListNodeList(emptyList())

    override fun getFirstChild(): Node? {
        return null
    }

    override fun getLastChild(): Node? {
        return null
    }

    override fun getPreviousSibling(): Node? {
        return null
    }

    override fun getNextSibling(): Node? {
        return null
    }

    override fun getAttributes(): NamedNodeMap? {
        return null
    }

    override fun getOwnerDocument(): Document? {
        return null
    }

    override fun hasChildNodes(): Boolean {
        return false
    }

    override fun cloneNode(deep: Boolean): Node? {
        return null
    }

    override fun normalize() {}
    override fun isSupported(feature: String, version: String): Boolean {
        return false
    }

    override fun getNamespaceURI(): String? {
        return null
    }

    override fun getPrefix(): String? {
        return null
    }

    override fun getLocalName(): String? {
        return null
    }

    /** DOM Level 3 */
    override fun getBaseURI(): String? {
        return null
    }

    override fun hasAttributes(): Boolean {
        return false
    }

    // setter methods
    @Throws(DOMException::class)
    override fun setNodeValue(nodeValue: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun insertBefore(newChild: Node, refChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun replaceChild(newChild: Node, oldChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun removeChild(oldChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun appendChild(newChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun setPrefix(prefix: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun compareDocumentPosition(other: Node): Short {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun getTextContent() = text

    @Throws(DOMException::class)
    override fun setTextContent(textContent: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isSameNode(other: Node): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun lookupPrefix(namespaceURI: String): String {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isDefaultNamespace(namespaceURI: String): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun lookupNamespaceURI(prefix: String): String {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isEqualNode(arg: Node): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun getFeature(feature: String, version: String): Any? {
        return null
    }

    override fun setUserData(key: String, data: Any, handler: UserDataHandler): Any {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun getUserData(key: String): Any? {
        return null
    }
}

private class FormattedStringContainer(private val childNode: Node) : Node {
    // getter methods
    override fun getNodeName(): String? {
        return null
    }

    @Throws(DOMException::class)
    override fun getNodeValue(): String? {
        return null
    }

    override fun getNodeType(): Short {
        return Node.ELEMENT_NODE
    }

    override fun getParentNode(): Node? {
        return null
    }

    override fun getChildNodes(): NodeList? = ListNodeList(listOf(childNode))

    override fun getFirstChild(): Node? {
        return childNode
    }

    override fun getLastChild(): Node? {
        return childNode
    }

    override fun getPreviousSibling(): Node? {
        return null
    }

    override fun getNextSibling(): Node? {
        return null
    }

    override fun getAttributes(): NamedNodeMap? {
        return null
    }

    override fun getOwnerDocument(): Document? {
        return null
    }

    override fun hasChildNodes(): Boolean {
        return false
    }

    override fun cloneNode(deep: Boolean): Node? {
        return null
    }

    override fun normalize() {}
    override fun isSupported(feature: String, version: String): Boolean {
        return false
    }

    override fun getNamespaceURI(): String? {
        return ReqIfQNames.THE_VALUE.namespaceURI
    }

    override fun getPrefix(): String? {
        return ReqIfQNames.THE_VALUE.prefix
    }

    override fun getLocalName(): String? {
        return ReqIfQNames.THE_VALUE.localPart
    }

    /** DOM Level 3 */
    override fun getBaseURI(): String? {
        return null
    }

    override fun hasAttributes(): Boolean {
        return false
    }

    // setter methods
    @Throws(DOMException::class)
    override fun setNodeValue(nodeValue: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun insertBefore(newChild: Node, refChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun replaceChild(newChild: Node, oldChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun removeChild(oldChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun appendChild(newChild: Node): Node {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun setPrefix(prefix: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun compareDocumentPosition(other: Node): Short {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun getTextContent(): String {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    @Throws(DOMException::class)
    override fun setTextContent(textContent: String) {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isSameNode(other: Node): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun lookupPrefix(namespaceURI: String): String {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isDefaultNamespace(namespaceURI: String): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun lookupNamespaceURI(prefix: String): String {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun isEqualNode(arg: Node): Boolean {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun getFeature(feature: String, version: String): Any? {
        return null
    }

    override fun setUserData(key: String, data: Any, handler: UserDataHandler): Any {
        throw DOMException(DOMException.NOT_SUPPORTED_ERR, "Method not supported")
    }

    override fun getUserData(key: String): Any? {
        return null
    }
}

private class ListNodeList(private val nodes: List<Node>) : NodeList {
    override fun item(index: Int): Node {
        return nodes[index]
    }

    override fun getLength() = nodes.size
}