package org.somda.reqif.ieee.datatype

enum class IeeeStandardType(reqifName: String) {
    UNSPECIFIED("Unspecified"),
    STANDARD("Standard"),
    GUIDE("Guide"),
    RECOMMENDED_PRACTICE("RecommendedPractice")
}