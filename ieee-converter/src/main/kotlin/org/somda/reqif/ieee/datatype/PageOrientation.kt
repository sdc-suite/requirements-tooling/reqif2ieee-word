package org.somda.reqif.ieee.datatype

enum class PageOrientation(val reqIfName: String) {
    PORTRAIT("Portrait"),
    LANDSCAPE("Landscape");

    companion object {
        private val map = values().associateBy(PageOrientation::reqIfName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
            ?: throw Exception("Enum $reqIfName could not be mapped to ${PageOrientation::class.java}")
    }
}