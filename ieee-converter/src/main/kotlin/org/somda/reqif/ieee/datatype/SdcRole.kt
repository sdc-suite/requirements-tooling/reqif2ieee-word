package org.somda.reqif.ieee.datatype

enum class SdcRole(val reqIfName: String) {
    NOT_APPLICABLE("n/a"),
    PARTICIPANT("Participant"),
    PROVIDER("Provider"),
    CONSUMER("Consumer");

    companion object {
        private val map = values().associateBy(SdcRole::reqIfName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
            ?: throw Exception("Enum $reqIfName could not be mapped to ${SdcRole::class.java}")
    }
}