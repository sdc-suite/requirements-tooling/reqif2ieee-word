package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.WordSimpleStringRenderer
import org.somda.reqif.ieee.spectype.obj.AbstractType
import org.somda.reqif.tree.ReqIfNode

class AbstractHook(private val abstractText: Collection<ReqIfNode>) :
    BookmarkHook {
    override fun label() = ObjectTypeName.ABSTRACT.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (abstractText.isNotEmpty()) {
            abstractText.iterator().next().theObject.let {
                if (it is AbstractType) {
                    WordSimpleStringRenderer(it.value.text)
                        .renderTo(bookmark.parent as ContentAccessor)
                }
            }
        }
    }
}