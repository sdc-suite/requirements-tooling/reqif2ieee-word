package org.somda.reqif.ieee.hook

import org.docx4j.jaxb.Context
import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.rendering.NestedSectionsRendering
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.tree.ReqIfNode

class AnnexHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {
    override fun label() = "Annex"

    override fun callback(bookmark: CTBookmark) {
        val bibAnnexHeading = bookmark.parent as P
        val topLevelContentAccessor = bibAnnexHeading.parent as ContentAccessor
        val firstParagraph = Context.getWmlObjectFactory().createP()
        topLevelContentAccessor.content.add(
            topLevelContentAccessor.content.indexOf(bibAnnexHeading),
            firstParagraph
        )
        NestedSectionsRendering(
            firstParagraph = firstParagraph,
            reqIfNodes = reqIfNodes,
            renderers = renderers,
            createPagebreaks = false,
            depthOffset = 0
        )
    }
}