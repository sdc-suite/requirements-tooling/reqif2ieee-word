package org.somda.reqif.ieee.hook

import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.rendering.WordRenderer
import org.somda.reqif.ieee.spectype.obj.BibliographicItemType
import org.somda.reqif.tree.ReqIfNode

class BibliographicItemHook(
    reqIfNodes: Collection<ReqIfNode>,
    renderers: Renderers
) : BibliographyHook(reqIfNodes, renderers, { (it as BibliographicItemType).value.name }) {
    override fun label() = ObjectTypeName.BIBLIOGRAPHIC_ITEM.reqifName
}