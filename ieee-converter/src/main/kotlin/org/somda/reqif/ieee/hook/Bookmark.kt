package org.somda.reqif.ieee.hook

import org.somda.reqif.ieee.rendering.xhtml.Anchor

data class Bookmark(val name: String, val label: String) {
    companion object {
        fun sanitizeName(name: String) = name.replace("[\\s]+".toRegex(), "_").trim()
    }

    fun isValid(): Boolean {
        val split = name.split(delimiters = *arrayOf(":"), limit = 2)
        return when (split.size) {
            2 -> split[1].isNotEmpty()
            else -> true
        }
    }
}