package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.rendering.WordSimpleStringRenderer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale

class DocumentDateHook : BookmarkHook {

    override fun label() = "DocumentDate"

    override fun callback(bookmark: CTBookmark) {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.US)
        val documentDate = current.format(formatter)
        
        documentDate?.also {
            WordSimpleStringRenderer(it)
                .renderTo(bookmark.parent as ContentAccessor)
        }
    }
}
