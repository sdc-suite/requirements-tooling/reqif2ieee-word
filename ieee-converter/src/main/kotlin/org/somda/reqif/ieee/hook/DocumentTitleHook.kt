package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.rendering.WordSimpleStringRenderer

class DocumentTitleHook(
    private val documentTitle: String?
) : BookmarkHook {

    override fun label() = "DocumentTitle"

    override fun callback(bookmark: CTBookmark) {
        documentTitle?.also {
            WordSimpleStringRenderer(it)
                .renderTo(bookmark.parent as ContentAccessor)
        }
    }
}
