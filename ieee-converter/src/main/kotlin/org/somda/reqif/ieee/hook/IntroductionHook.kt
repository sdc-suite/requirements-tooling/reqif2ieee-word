package org.somda.reqif.ieee.hook

import org.docx4j.jaxb.Context
import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.rendering.createParagraph
import org.somda.reqif.tree.ReqIfNode

class IntroductionHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {
    override fun label() = ObjectTypeName.INTRODUCTION.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (reqIfNodes.isEmpty()) {
            System.err.println("No introduction found")
            return
        }

        val introductionElements = reqIfNodes.iterator().next().children
        val firstParagraph = bookmark.parent as P
        val topLevelContentAccessor = firstParagraph.parent as ContentAccessor
        var offset = topLevelContentAccessor.content.indexOf(firstParagraph)
        topLevelContentAccessor.content.removeAt(offset)


        for ((index, reqIfNode) in introductionElements.withIndex()) {
            val nextParagraph = topLevelContentAccessor.createParagraph()
            topLevelContentAccessor.content.add(offset + index, nextParagraph)
            val sizeBeforeRendering = topLevelContentAccessor.content.size
            renderers.paragraphRenderer(reqIfNode).renderTo(nextParagraph!!)
            // this considers any paragraphs being creating during rendering
            offset += topLevelContentAccessor.content.size - sizeBeforeRendering
        }
    }
}