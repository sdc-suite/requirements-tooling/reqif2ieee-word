package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.NestedSectionsRendering
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.tree.ReqIfNode

class MainContentHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {
    override fun label() = ObjectTypeName.HEADING.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (reqIfNodes.isEmpty()) {
            return
        }
        NestedSectionsRendering(
            firstParagraph = bookmark.parent as P,
            reqIfNodes = reqIfNodes,
            renderers = renderers,
            createPagebreaks = true,
            depthOffset = (reqIfNodes.iterator().next().parent?.theObject?.depth ?: -1) + 1
        )
    }
}