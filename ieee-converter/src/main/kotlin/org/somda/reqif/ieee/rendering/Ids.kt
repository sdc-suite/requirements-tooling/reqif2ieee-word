package org.somda.reqif.ieee.rendering

import java.math.BigInteger

// get unique id numbers for Word IDs (not thread-safe)
class Ids {
     companion object {
         private var currentId = 80000 // random offset
         fun next() = currentId++
         fun nextAsBigInt(): BigInteger = BigInteger.valueOf(next().toLong())
     }
}