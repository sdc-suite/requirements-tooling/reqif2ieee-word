package org.somda.reqif.ieee.rendering

import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ListType
import org.somda.reqif.ieee.spectype.obj.*
import org.somda.reqif.tree.ReqIfNode
import java.lang.Exception


class NestedSectionsRendering(
    firstParagraph: P,
    reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers,
    createPagebreaks: Boolean,
    private val depthOffset: Int
) {
    init {
        if (reqIfNodes.isEmpty()) {
            System.err.println("No sections found")
        }

        val nextParagraph = NextParagraph(firstParagraph)
        reqIfNodes.forEach {
            renderReqIfNode(it, nextParagraph)
            if (it.theObject is AnnexType) {
                if (createPagebreaks) {
                    nextParagraph.add { createPagebreak() }
                }
            }
        }

        nextParagraph.removeOffsetParagraph()
    }

    private fun renderReqIfNode(reqIfNode: ReqIfNode, nextParagraph: NextParagraph) {
        when (reqIfNode.theObject) {
            is HeadingType -> {
                nextParagraph.createAndAdd { renderers.headingRenderer(reqIfNode, reqIfNode.theObject, depthOffset).renderTo(it) }
                reqIfNode.children.forEach { renderReqIfNode(it, nextParagraph) }
            }
            is AnnexType -> {
                nextParagraph.createAndAdd { renderers.annexRenderer(reqIfNode, reqIfNode.theObject, depthOffset).renderTo(it) }
                reqIfNode.children.forEach { renderReqIfNode(it, nextParagraph) }
            }
            is ParagraphType, is ComputerCodeType -> {
                nextParagraph.createAndAdd { renderers.paragraphRenderer(reqIfNode).renderTo(it) }
            }
            is PictureType -> {
                nextParagraph.createAndAdd { renderers.pictureRenderer(reqIfNode).renderTo(it) }
            }
            is TableType -> {
                nextParagraph.createAndAdd { renderers.tableRenderer(reqIfNode).renderTo(it) }
            }
            is IcsTableType -> {
                nextParagraph.createAndAdd { renderers.icsTableRenderer(reqIfNode).renderTo(it) }
            }
            is ListItemType -> {
                when (reqIfNode.theObject.value.listType) {
                    ListType.NUMBERED, ListType.LATIN -> {
                        nextParagraph.createAndAdd { renderers.enumerationRenderer(reqIfNode).renderTo(it) }
                        reqIfNode.children.forEach { renderReqIfNode(it, nextParagraph) }
                    }
                    ListType.DASHED, ListType.BULLETED -> {
                        nextParagraph.createAndAdd { renderers.paragraphRenderer(reqIfNode).renderTo(it) }
                    }
                }
            }
            is RequirementType -> {
                if (!reqIfNode.theObject.value.text.simpleString()
                        .contains(reqIfNode.theObject.value.level.reqIfName)
                ) {
                    throw Exception("Requirement ${reqIfNode.theObject} does not contain the expected " +
                            "requirement keyword ${reqIfNode.theObject.value.level.reqIfName}")
                }
                nextParagraph.createAndAdd { renderers.requirementRenderer(reqIfNode).renderTo(it) }
            }
            else -> Unit
        }
    }
}