package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P

class NextParagraph(private val offsetParagraph: P) {
    private var topLevelContentAccessor: ContentAccessor = offsetParagraph.parent as ContentAccessor
    private var offset = topLevelContentAccessor.content.indexOf(offsetParagraph)
    private var count = 0

    fun createAndAdd(render: (paragraph: P) -> Unit) {
        val sizeBefore = topLevelContentAccessor.content.size
        val p = topLevelContentAccessor.createParagraph()
        topLevelContentAccessor.content.add(offset + count, p)

        render(p)

        val sizeAfter = topLevelContentAccessor.content.size
        count += sizeAfter - sizeBefore
    }

    fun add(render: () -> P) {
        topLevelContentAccessor.content.add(offset + count++, render())
    }

    fun removeOffsetParagraph() {
        topLevelContentAccessor.content.remove(offsetParagraph)
    }
}