package org.somda.reqif.ieee.rendering

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.PPr
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.xhtml.predefinedPlugins
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import java.io.File

data class Renderers(
    val document: WordprocessingMLPackage,
    val inputDir: File,
    val bookmarks: Map<String, Bookmark> = emptyMap(),
    val requirements: List<ReqIfNode>,
    val icsMinCellHeight: Int?,
    val icsHideFeature: Boolean,
    val runRenderer: (reqIfNode: ReqIfNode, node: Node, rPr: RPr, PPr) -> WordRenderer = { reqIfNode: ReqIfNode, node: Node, rPr: RPr, pPr: PPr ->
        WordRunRenderer(reqIfNode, node, rPr, pPr, predefinedPlugins(document, bookmarks, inputDir))
    },
    val paragraphRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordParagraphRenderer(reqIfNode, predefinedPlugins(document, bookmarks, inputDir), runRenderer)
    },
    val pictureRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordPictureRenderer(reqIfNode, predefinedPlugins(document, bookmarks, inputDir), runRenderer)
    },
    val tableRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordTableRenderer(reqIfNode, predefinedPlugins(document, bookmarks, inputDir), runRenderer)
    },
    val icsTableRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordIcsTableRenderer(
            reqIfNode,
            requirements,
            predefinedPlugins(document, bookmarks, inputDir),
            runRenderer,
            icsMinCellHeight,
            icsHideFeature
        )
    },
    val headingRenderer: (reqIfNode: ReqIfNode, obj: ObjectType, depthOffset: Int) -> WordRenderer = { reqIfNode: ReqIfNode, obj: ObjectType, depthOffset: Int ->
        WordHeadingRenderer(
            reqIfNode,
            obj,
            WordConstants.STYLE_TEMPLATE_HEADING_LEVEL,
            runRenderer,
            depthOffset
        )
    },
    val annexRenderer: (reqIfNode: ReqIfNode, obj: ObjectType, depthOffset: Int) -> WordRenderer = { reqIfNode: ReqIfNode, obj: ObjectType, depthOffset: Int ->
        WordAnnexRenderer(reqIfNode, obj, runRenderer, depthOffset)
    },
    val annexHeadingRenderer: (ReqIfNode, ObjectType, Int) -> WordRenderer = { reqIfNode: ReqIfNode, obj: ObjectType, depthOffset: Int ->
        WordHeadingRenderer(
            reqIfNode,
            obj,
            WordConstants.STYLE_TEMPLATE_ANNEX_HEADING_LEVEL,
            runRenderer,
            depthOffset
        )
    },
    val noteRenderer: (List<ReqIfNode>, Int) -> WordRenderer = { reqIfNodes: List<ReqIfNode>, targetPosition: Int ->
        WordNoteRenderer(reqIfNodes, targetPosition, runRenderer, document)
    },
    val requirementRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordRequirementRenderer(reqIfNode, predefinedPlugins(document, bookmarks, inputDir), runRenderer, noteRenderer)
    },
    val enumerationRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordEnumerationRenderer(reqIfNode, predefinedPlugins(document, bookmarks, inputDir), runRenderer)
    },
    val definitionRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordDefinitionRenderer(reqIfNode, paragraphRenderer, noteRenderer)
    },
    val abbreviationRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordAbbreviationRenderer(reqIfNode, paragraphRenderer)
    },
    val bibliographicItemRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordBibliographicItemRenderer(reqIfNode, noteRenderer, runRenderer)
    },
    val normativeReferenceRenderer: (reqIfNode: ReqIfNode) -> WordRenderer = { reqIfNode: ReqIfNode ->
        WordNormativeReferenceRenderer(reqIfNode, noteRenderer, runRenderer)
    }
)