package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.*
import org.somda.reqif.ieee.datatype.Normativity
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.spectype.obj.AnnexType
import org.somda.reqif.ieee.spectype.obj.Heading
import org.somda.reqif.ieee.spectype.obj.HeadingType
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName

open class WordAnnexRenderer(
    private val reqIfNode: ReqIfNode,
    private val obj: ObjectType,
    runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer,
    depthOffset: Int
) : WordHeadingRenderer(
    reqIfNode,
    obj.run {
        when (this) {
            is AnnexType -> HeadingType(obj.depth, Heading(value.heading, value.bookmark))
            else -> throw Exception("Wrong type passed to WordAnnexRenderer: ${this::class}")
        }
    },
    WordConstants.STYLE_TEMPLATE_ANNEX_HEADING_LEVEL,
    runRenderer,
    depthOffset
) {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val paragraph = contentAccessor as P

        val annexInfo = when ((obj as AnnexType).value.normativity) {
            Normativity.INFORMATIVE -> "(informative)"
            Normativity.NORMATIVE -> "(normative)"
        }

        paragraph.content.add(createLinebreak())
        val rPr = Context.getWmlObjectFactory().createRPr()
        val value = BooleanDefaultTrue()
        value.isVal = false
        rPr.b = value
        paragraph.content.add(createWordRun(annexInfo, rPr))
        paragraph.content.add(createLinebreak())

        return super.renderTo(contentAccessor)
    }
}