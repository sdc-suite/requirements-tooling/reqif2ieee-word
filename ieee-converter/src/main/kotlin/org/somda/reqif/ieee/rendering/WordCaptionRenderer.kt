package org.somda.reqif.ieee.rendering

import org.docx4j.wml.*
import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import java.util.*
import javax.xml.namespace.QName
import kotlin.collections.HashMap


open class WordCaptionRenderer(
    private val reqIfNode: ReqIfNode,
    private val type: CaptionType,
    private val text: FormattedString,
    private val bookmark: Bookmark,
    private val annexLetter: String?,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer
) : WordRenderer {

    private val factory = ObjectFactory()

    private companion object {
        val annexCaptionNumbers: MutableMap<CaptionType, MutableMap<String, Int>> = EnumMap(CaptionType::class.java)
        init {
            annexCaptionNumbers[CaptionType.FIGURE] = HashMap()
            annexCaptionNumbers[CaptionType.TABLE] = HashMap()
        }
    }

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val paragraph = contentAccessor as P
        val parentAccessor = paragraph.parent as ContentAccessor
        val newParagraph = if (annexLetter == null) {
            createRegularCaption(type, text, parentAccessor)
        } else {
            createAnnexCaption(type, annexLetter, calculateNextCaptionNumber(type, annexLetter), text, parentAccessor)
        }

        newParagraph.parent = parentAccessor
        parentAccessor.content.add(parentAccessor.content.indexOf(paragraph) + 1, newParagraph)

        return newParagraph
    }

    private fun createRegularCaption(type: CaptionType, captionText: FormattedString, parent: ContentAccessor): P {
        val p = factory.createP()
        p.parent = parent
        p.pPr = createPPr(type.regularStyle, JcEnumeration.CENTER)
        p.appendBookmark(bookmark, createWordRun("—"))
        elementPlugins[text.qName()]?.handle(
            PluginContext(
                reqIfNode,
                captionText,
                p,
                RPr(),
                defaultPPr(),
                runRenderer
            )
        )
        return p
    }

    private fun createAnnexCaption(
        type: CaptionType,
        annexLetter: String,
        captionNumber: Int?,
        captionText: FormattedString,
        parent: ContentAccessor
    ): P {
        val p = factory.createP()
        p.parent = parent
        p.pPr = createPPr(WordConstants.STYLE_CAPTION)

        p.appendBookmark(bookmark) {
            it.content.add(createWordRun("${type.label} "))

            it.content.add(createFieldCharBeginRun())
            it.content.add(createInstructionText(" STYLEREF 1 \\s "))
            it.content.add(createFieldCharSeparateRun())
            it.content.add(createWordRun(annexLetter))
            it.content.add(createFieldCharEndRun())

            it.content.add(createWordRun("."))

            it.content.add(createFieldCharBeginRun())
            it.content.add(createInstructionText(" SEQ Figure \\* ARABIC \\s 1 "))
            it.content.add(createFieldCharSeparateRun())
            it.content.add(createWordRun(captionNumber?.toString() ?: "1"))
            it.content.add(createFieldCharEndRun())
        }

        p.content.add(createWordRun("—"))

        elementPlugins[text.qName()]?.handle(
            PluginContext(
                reqIfNode,
                captionText,
                p,
                RPr(),
                defaultPPr(),
                runRenderer
            )
        )

        return p
    }

    private fun calculateNextCaptionNumber(type: CaptionType, letter: String): Int {
        val map = annexCaptionNumbers[type]!!
        val nextNo = map[letter]?.plus(1) ?: 1
        map[letter] = nextNo
        return nextNo
    }
}