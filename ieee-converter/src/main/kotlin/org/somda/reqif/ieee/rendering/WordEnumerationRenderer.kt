package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.*
import org.somda.reqif.ieee.datatype.ListType
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.spectype.obj.ListItemType
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName


open class WordEnumerationRenderer(
    private val reqIfNode: ReqIfNode,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer
) : WordRenderer {
    companion object {
        fun isNumberedListItem(obj: ObjectType) =
            obj is ListItemType && (obj.value.listType == ListType.NUMBERED ||
                    obj.value.listType == ListType.LATIN)
    }

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (!isNumberedListItem(reqIfNode.theObject)) {
            System.err.println("Object ${reqIfNode.theObject} is not a numbered enumeration")
            return contentAccessor
        }

        val obj = reqIfNode.theObject as ListItemType
        val paragraph = contentAccessor as P
        paragraph.pPr = createPPr(
            WordConstants.STYLE_TEMPLATE_NUMBERED_LIST_LEVEL.format(calcEnumDepth(reqIfNode)), JcEnumeration.LEFT)
        return elementPlugins[obj.value.text.qName()]?.handle(
            PluginContext(
                reqIfNode,
                obj.value.text,
                paragraph,
                Context.getWmlObjectFactory().createRPr(),
                defaultPPr(),
                runRenderer
            )
        ) ?: contentAccessor
    }

    private fun calcEnumDepth(reqIfNode: ReqIfNode): Int {
        var currentNode = reqIfNode
        while (currentNode.parent != null && isNumberedListItem(currentNode.parent!!.theObject)) {
            currentNode = currentNode.parent!!
        }
        return reqIfNode.theObject.depth - currentNode.theObject.depth + 1
    }
}