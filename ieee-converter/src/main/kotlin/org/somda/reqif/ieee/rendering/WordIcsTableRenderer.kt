package org.somda.reqif.ieee.rendering

import org.docx4j.XmlUtils
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.wml.*
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.xhtml.XhtmlConstants
import org.somda.reqif.ieee.spectype.obj.AnnexType
import org.somda.reqif.ieee.spectype.obj.IcsTable
import org.somda.reqif.ieee.spectype.obj.IcsTableType
import org.somda.reqif.ieee.spectype.obj.RequirementType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Element
import org.w3c.dom.Node
import javax.xml.namespace.QName

class WordIcsTableRenderer(
    private val reqIfNode: ReqIfNode,
    private val requirements: List<ReqIfNode>,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer,
    private val minCellHeight: Int?,
    private val hideFeature: Boolean
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val parentAccessor = (contentAccessor as P).parent as ContentAccessor

        // info: no landscape support for ICS tables

        if (reqIfNode.theObject !is IcsTableType) {
            throw Exception("Expected an IcsTable, but found ${reqIfNode.theObject.javaClass}")
        }

        val theValue = reqIfNode.theObject.value.text
        val tableId = theValue.simpleString().trim()

        while (theValue.hasChildNodes()) {
            theValue.removeChild(theValue.firstChild);
        }

        val table = createElement(XhtmlConstants.ELEM_TABLE)
        theValue.appendChild(table)

        insertHeaderRow(table)

        requirements
            .filter { it.theObject is RequirementType }
            .map { (it.theObject as RequirementType).value }
            .filter { it.icsGroup == tableId }
            .forEach {
                val icsFeature = dashIfEmpty(it.icsFeature)
                val icsStatus = dashIfEmpty(it.icsStatus)

                val cells = ArrayList<String>()
                cells.add("ICS-${it.number}")
                if (!hideFeature) {
                    cells.add(icsFeature)
                }
                cells.addAll(listOf("[${it.shortBookmark()}]", icsStatus, "", ""))
                insertDataRow(
                    table,
                    cells
                )
            }

        var targetAccessor = WordCaptionRenderer(
            reqIfNode,
            CaptionType.TABLE,
            reqIfNode.theObject.value.caption,
            IcsTable.bookmarkFor(reqIfNode.theObject.value.bookmark),
            findAnnexLetter(),
            elementPlugins,
            runRenderer
        ).renderTo(contentAccessor)

        val pTable = parentAccessor.createParagraph()
        val index = parentAccessor.content.indexOf(targetAccessor)
        parentAccessor.content.add(index + 1, pTable)
        targetAccessor = WordStyledParagraphRenderer(
            reqIfNode,
            theValue,
            contentAccessor.pPr ?: reqIfNode.theObject.toWordPPr(),
            elementPlugins,
            runRenderer
        ).renderTo(pTable)

        // original content accessor and generated placeholder paragraph left empty; remove to avoid blank paragraphs
        parentAccessor.content.remove(contentAccessor)
        parentAccessor.content.remove(pTable)

        return targetAccessor
    }

    private fun dashIfEmpty(text: String) = when (text.trim().isEmpty()) {
        true -> "—"
        false -> text
    }

    private fun findAnnexLetter(): String? {
        var currentNode = reqIfNode
        while (currentNode.parent != null) {
            val nonNullParent = currentNode.parent!!
            if (nonNullParent.theObject is AnnexType) {
                return nonNullParent.theObject.value.anticipatedLetter.toString()
            }
            currentNode = nonNullParent
        }
        return null
    }

    private fun makeLandscapeP(): P {
        val pAsXml = "<w:p ${Namespaces.W_NAMESPACE_DECLARATION}><w:pPr>" +
                "  <w:pStyle w:val=\"${WordConstants.STYLE_PARAGRAPH}\"/>" +
                "  <w:sectPr>" +
                "    <w:footnotePr>" +
                "      <w:numRestart w:val=\"eachSect\"/>" +
                "    </w:footnotePr>" +
                "    <w:pgSz w:w=\"15840\" w:h=\"12240\" w:orient=\"landscape\" w:code=\"1\"/>" +
                "    <w:pgMar w:top=\"1800\" w:right=\"1440\" w:bottom=\"1800\" w:left=\"1440\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/>" +
                "    <w:lnNumType w:countBy=\"1\"/>" +
                "    <w:cols w:space=\"720\"/>" +
                "    <w:docGrid w:linePitch=\"360\"/>" +
                "  </w:sectPr>" +
                "</w:pPr></w:p>"

        return XmlUtils.unmarshalString(pAsXml) as P
    }

    private fun makePortraitSectPr(): SectPr {
        val sectPrAsXml = "<w:sectPr ${Namespaces.W_NAMESPACE_DECLARATION} xmlns:test='http://test' test:a='test'>" +
                "<w:footnotePr>" +
                "<w:numRestart w:val=\"eachSect\"/>" +
                "</w:footnotePr>" +
                "<w:type w:val=\"continuous\"/>" +
                "<w:pgSz w:w=\"12240\" w:h=\"15840\" w:code=\"1\"/>" +
                "<w:pgMar w:top=\"1440\" w:right=\"1800\" w:bottom=\"1440\" w:left=\"1800\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/>" +
                "<w:lnNumType w:countBy=\"1\"/>" +
                "<w:cols w:space=\"720\"/>" +
                "<w:docGrid w:linePitch=\"360\"/>" +
                "</w:sectPr>"

        return XmlUtils.unmarshalString(sectPrAsXml) as SectPr
    }

    private fun createElement(qName: QName): Element =
        (reqIfNode.theObject as IcsTableType).value.text.ownerDocument.createElementNS(
            qName.namespaceURI,
            qName.localPart
        )

    private fun insertHeaderRow(table: Element) {
        val tr = createElement(XhtmlConstants.ELEM_TABLE_ROW)
        table.appendChild(tr)

        val headers = ArrayList<Pair<String, Int>>()
        headers.add(Pair("Index", 15))
        var supportWidth = 10
        var commentWidth = 30
        if (!hideFeature) {
            headers.add(Pair("Feature", 20))
        } else {
            supportWidth += 10
            commentWidth += 10
        }
        headers.addAll(
            listOf(
                Pair("Reference", 15),
                Pair("Status", 10),
                Pair("Support", supportWidth),
                Pair("Comment", commentWidth)
            )
        )

        headers.forEach {
            val th = createElement(XhtmlConstants.ELEM_TABLE_HEADER)
            tr.appendChild(th)
            th.textContent = it.first

            val width = table.ownerDocument.createAttribute("width")
            width.textContent = "${it.second}%"
            th.attributes.setNamedItem(width)
        }
    }

    private fun insertDataRow(table: Element, content: List<String>) {
        val tr = createElement(XhtmlConstants.ELEM_TABLE_ROW)
        val height = tr.ownerDocument.createAttribute("height")
        minCellHeight?.also {
            height.textContent = "${it}twips"
        }
        tr.setAttributeNode(height)
        table.appendChild(tr)
        content.forEach {
            val td = createElement(XhtmlConstants.ELEM_TABLE_DATA)
            tr.appendChild(td)
            td.textContent = it
        }
    }
}