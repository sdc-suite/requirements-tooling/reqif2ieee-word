package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor

interface WordRenderer {
    fun renderTo(contentAccessor: ContentAccessor): ContentAccessor
}