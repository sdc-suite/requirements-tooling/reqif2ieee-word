package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.*
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.spectype.obj.NoteType
import org.somda.reqif.ieee.spectype.obj.RequirementType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName


open class WordRequirementRenderer(
    private val reqIfNode: ReqIfNode,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer,
    private val noteRenderer: (List<ReqIfNode>, Int) -> WordRenderer
) : WordRenderer {

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (reqIfNode.theObject !is RequirementType) {
            System.err.println("Object ${reqIfNode.theObject} is not a requirement")
            return contentAccessor
        }

        val obj = reqIfNode.theObject
        val paragraph = contentAccessor as P
        val factory: ObjectFactory = Context.getWmlObjectFactory()
        val parentStyle = factory.createRPr()
        paragraph.pPr = createPPr(WordConstants.STYLE_REQUIREMENT, JcEnumeration.LEFT)
        paragraph.appendBookmark(obj.value.bookmark(), createWordRun(obj.value.tag(), parentStyle))
        paragraph.content.add(createWordRun(": ", parentStyle))
        var cAccess = elementPlugins[obj.value.text.qName()]?.handle(
            PluginContext(
                reqIfNode,
                obj.value.text,
                paragraph,
                parentStyle,
                createPPr(WordConstants.STYLE_REQUIREMENT),
                runRenderer
            )
        ) ?: contentAccessor

        val parent = paragraph.parent as ContentAccessor
        cAccess = noteRenderer(notes(reqIfNode), parent.content.indexOf(cAccess)).renderTo(parent)
        return cAccess
    }

    private fun notes(reqIfNode: ReqIfNode) = reqIfNode.children.filter { it.theObject is NoteType }
}