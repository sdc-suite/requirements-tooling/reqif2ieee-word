package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.w3c.dom.Node

class WordSimpleStringRenderer : WordRenderer {
    private val node: Node?
    private val string: String?

    constructor(node: Node?) {
        this.node = node
        this.string = null
    }

    constructor(string: String?) {
        this.node = null
        this.string = string
    }

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        node?.let {
            val simpleStr = it.simpleString()
            contentAccessor.content.add(createWordRun(simpleStr))
        }
        string?.let {
            contentAccessor.content.add(createWordRun(it))
        }
        return contentAccessor
    }
}