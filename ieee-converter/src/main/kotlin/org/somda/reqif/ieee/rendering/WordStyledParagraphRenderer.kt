package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.docx4j.wml.PPr
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName

open class WordStyledParagraphRenderer(
    private val reqIfNode: ReqIfNode,
    private val text: FormattedString,
    private val style: PPr,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr, PPr) -> WordRenderer
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val paragraph = contentAccessor as P

        paragraph.pPr = style
        return elementPlugins[text.qName()]?.handle(
            PluginContext(
                reqIfNode,
                text,
                paragraph,
                RPr(),
                defaultPPr(),
                runRenderer
            )
        ) ?: contentAccessor
    }
}