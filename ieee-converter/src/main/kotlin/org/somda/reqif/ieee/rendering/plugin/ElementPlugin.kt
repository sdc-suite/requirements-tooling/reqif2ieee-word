package org.somda.reqif.ieee.rendering.plugin

import org.docx4j.XmlUtils
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.relationships.ObjectFactory
import org.docx4j.relationships.Relationship
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.appendBookmarkReference
import org.somda.reqif.ieee.rendering.appendNumberedBookmarkReference
import org.somda.reqif.ieee.rendering.appendReferenceToHeading
import org.somda.reqif.ieee.rendering.createWordRun
import org.somda.reqif.ieee.rendering.xhtml.Anchor
import org.somda.reqif.ieee.spectype.obj.*
import java.net.MalformedURLException
import java.net.URL
import javax.xml.namespace.QName

interface ElementPlugin {
    companion object {
        fun appendPlainRun(
            document: MainDocumentPart,
            bookmarks: Map<String, Bookmark>,
            text: String,
            pluginContext: PluginContext
        ) {
            if (text.isBlank()) {
                return
            }

            val str = reduceMultipleWhitespaces(text)
            var openBracket = -1
            var copyOffset = 0
            for (i in str.indices) {
                when (str[i]) {
                    '[' -> openBracket = i
                    ']' -> if (openBracket != -1) {
                        appendPart(str.substring(copyOffset, openBracket), pluginContext)
                        copyOffset = i + 1
                        resolveReference(bookmarks, str.substring(openBracket + 1, i), document, pluginContext)
                    }
                }
            }

            if (copyOffset < str.length) {
                appendPart(str.substring(copyOffset), pluginContext)
            }
        }

        private fun resolveReference(
            bookmarks: Map<String, Bookmark>,
            bookmarkName: String,
            document: MainDocumentPart,
            pluginContext: PluginContext
        ) {
            val autoResolvedBookmarkName = findAutoBookmark(bookmarks, bookmarkName) ?: bookmarkName

            var bookmark = findHeadingBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendReferenceToHeading(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process heading bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findAnchorBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process anchor bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findRequirementBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process requirement bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findNoteBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process note bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findTableBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendNumberedBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process table bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findAnnexTableBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process annex table bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findPictureBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendNumberedBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process picture bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findAnnexPictureBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process picture bookmark: '$autoResolvedBookmarkName'")
                return
            }
            bookmark = findBibBookmark(bookmarks, autoResolvedBookmarkName)
            if (bookmark != null) {
                appendPart("[", pluginContext)
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                appendPart("]", pluginContext)
                println("Process bookmark: '$autoResolvedBookmarkName'")
                return
            }
            try {
                val url = URL(autoResolvedBookmarkName)
                if (!url.toURI().isAbsolute) {
                    throw MalformedURLException()
                }
                println("Process hyperlink from brackets: '$autoResolvedBookmarkName'")
                pluginContext.contentAccessor.content.add(
                    createExternalHyperlink(
                        document,
                        autoResolvedBookmarkName,
                        autoResolvedBookmarkName
                    )
                )
            } catch (e: MalformedURLException) {
                System.err.println("Process unknown name in brackets: '$autoResolvedBookmarkName'")
                appendPart(autoResolvedBookmarkName, pluginContext)
            }
        }

        private fun appendPart(text: String, pluginContext: PluginContext) {
            pluginContext.contentAccessor.content.add(createWordRun(text, pluginContext.parentStyle))
        }

        private fun findBibBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String): Bookmark? {
            return listOf(
                NormativeReference.BOOKMARK_PREFIX,
                BibliographicItem.BOOKMARK_PREFIX
            ).map { "$it${Bookmark.sanitizeName(bookmarkName)}" }.firstOrNull { bookmarks.containsKey(it) }?.let {
                bookmarks[it]
            }
        }

        private fun findAutoBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String): String? {
            if (!bookmarkName.toLowerCase().startsWith("x:")) {
                return null
            }
            return listOf(
                Pair(::findAnchorBookmark, Anchor.BOOKMARK_PREFIX_SHORT),
                Pair(::findHeadingBookmark, Heading.BOOKMARK_PREFIX_SHORT),
                Pair(::findTableBookmark, Table.BOOKMARK_PREFIX_SHORT),
                Pair(::findAnnexTableBookmark, Table.BOOKMARK_PREFIX_SHORT),
                Pair(::findPictureBookmark, Picture.BOOKMARK_PREFIX_SHORT),
                Pair(::findAnnexPictureBookmark, Picture.BOOKMARK_PREFIX_SHORT),
                Pair(::findNoteBookmark, Note.BOOKMARK_PREFIX_SHORT)
            ).map {
                val bm = bookmarkName.replaceBefore(':', it.second)
                it.first(bookmarks, bm)?.let { bm }
            }.find { it != null }
        }

        private fun findAnchorBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Anchor.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findRequirementBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Requirement.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findNoteBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Note.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findHeadingBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Heading.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findTableBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Table.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findAnnexTableBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Table.expandShortAnnexBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findPictureBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Picture.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findAnnexPictureBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Picture.expandShortAnnexBookmark(bookmarkName)?.let { bookmarks[it] }


        fun createExternalHyperlink(document: MainDocumentPart, url: String, name: String): P.Hyperlink {
            // We need to add a relationship to word/_rels/document.xml.rels
            // but since its external, we don't use the
            // usual wordMLPackage.getMainDocumentPart().addTargetPart
            // mechanism
            val factory = ObjectFactory()
            val rel: Relationship = factory.createRelationship()
            rel.type = Namespaces.HYPERLINK
            rel.target = url
            rel.targetMode = "External"
            document.relationshipsPart.addRelationship(rel)

            // addRelationship sets the rel's @Id
            val hpl =
                "<w:hyperlink r:id=\"${rel.id}\" " +
                        "xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
                        "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">" +
                        "<w:r>" +
                        "<w:rPr>" +
                        "</w:rPr>" +
                        "<w:t>$name</w:t>" +
                        "</w:r>" +
                        "</w:hyperlink>"
            return XmlUtils.unmarshalString(hpl) as P.Hyperlink
        }

        fun reduceMultipleWhitespaces(text: String): String =
            text.replace(Regex("\\s+"), " ")

    }

    fun handledQName(): QName
    fun handle(pluginContext: PluginContext): ContentAccessor
}