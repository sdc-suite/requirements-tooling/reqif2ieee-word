package org.somda.reqif.ieee.rendering.plugin

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.PPr
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.rendering.WordRenderer
import org.somda.reqif.ieee.rendering.defaultPPr
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node

data class PluginContext(
    val reqIfNode: ReqIfNode,
    val source: Node,
    val contentAccessor: ContentAccessor,
    val parentStyle: RPr = RPr(),
    val defaultPStyle: PPr = defaultPPr(),
    val runRenderer: (reqIfNode: ReqIfNode, node: Node, parentStyle: RPr, defaultPStyle: PPr) -> WordRenderer
)

