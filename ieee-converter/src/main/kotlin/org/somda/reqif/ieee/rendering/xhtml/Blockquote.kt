package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.w3c.dom.Node


class Blockquote(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>
) : Div(document, bookmarks) {
    override fun handledQName() = XhtmlConstants.ELEM_BLOCKQUOTE
}