package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.jaxb.Context
import org.docx4j.wml.BooleanDefaultTrue
import org.docx4j.wml.RPr
import java.math.BigInteger

class Code : BasicStylePlugin() {
    override fun handledQName() = XhtmlConstants.ELEM_CODE
    override fun decorate(rPr: RPr) {
        val factory = Context.getWmlObjectFactory()
        rPr.rFonts = factory.createRFonts()
        val fontName = "Courier New"
        rPr.rFonts.ascii = fontName
        rPr.rFonts.hAnsi = fontName
        rPr.szCs = factory.createHpsMeasure()
        rPr.szCs.`val` = BigInteger.valueOf(24)
    }
}