package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.rendering.createLinebreak
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.rendering.qName
import org.w3c.dom.Node

class Linebreak : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_BREAKLINE
    override fun handle(pluginContext: PluginContext): ContentAccessor {
        // Ignore line break when followed by a list
        var nextSibling = pluginContext.source.nextSibling
        while (nextSibling != null) {
            if (nextSibling.nodeType == Node.ELEMENT_NODE) {
                if (nextSibling.qName() == XhtmlConstants.ELEM_UL) {
                    return pluginContext.contentAccessor
                }
                break
            }
            nextSibling = nextSibling.nextSibling
        }

        pluginContext.contentAccessor.content.add(createLinebreak())
        return pluginContext.contentAccessor
    }
}