package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ListType
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.*
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.spectype.obj.RequirementType
import org.w3c.dom.Node

abstract class ListHandler(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>,
    private val listType: ListType
) : ElementPlugin {
    override fun handle(pluginContext: PluginContext): ContentAccessor {
        val thisP = pluginContext.contentAccessor as P
        val parent = thisP.parent as ContentAccessor
        val originalStyle = thisP.pPr

        val listPPrStyle = when (listType) {
            ListType.DASHED, ListType.BULLETED -> when (pluginContext.reqIfNode.theObject) {
                is RequirementType -> WordConstants.STYLE_REQUIREMENT_UNORDERED_LIST
                else -> WordConstants.STYLE_UNORDERED_LIST
            }
            ListType.NUMBERED, ListType.LATIN -> when (pluginContext.reqIfNode.theObject) {
                is RequirementType -> WordConstants.STYLE_TEMPLATE_REQUIREMENT_NUMBERED_LIST_LEVEL.format(1)
                else -> WordConstants.STYLE_TEMPLATE_NUMBERED_LIST_LEVEL.format(1)
            }
        }
        val listPPr = createPPr(listPPrStyle, JcEnumeration.LEFT)

        var nextP = when (thisP.hasContent()) {
            true -> {
                val result = parent.createParagraph()
                parent.content.add(parent.content.indexOf(thisP) + 1, result)
                result
            }
            false -> thisP
        }

        for (i in 0 until pluginContext.source.childNodes.length) {
            val currentNode = pluginContext.source.childNodes.item(i)
            // look for first node element, which is supposed to be li - no support for nested lists yet
            if (currentNode.nodeType == Node.ELEMENT_NODE && currentNode.qName() == XhtmlConstants.ELEM_LI) {
                nextP.pPr = listPPr
                nextP = pluginContext.runRenderer(pluginContext.reqIfNode, currentNode, pluginContext.parentStyle, pluginContext.defaultPStyle)
                    .renderTo(nextP) as P
            }
        }

        return nextP
    }
}