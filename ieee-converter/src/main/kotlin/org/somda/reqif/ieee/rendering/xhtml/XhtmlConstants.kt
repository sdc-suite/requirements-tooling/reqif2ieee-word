package org.somda.reqif.ieee.rendering.xhtml

import javax.xml.namespace.QName

class XhtmlConstants {
    companion object {
        const val NAMESPACE_XHTML = "http://www.w3.org/1999/xhtml"
        const val PREFIX_XHTML = "xhtml"

        val ELEM_BOLD = QName(
            NAMESPACE_XHTML, "b", PREFIX_XHTML
        )

        val ELEM_ITALIC = QName(
            NAMESPACE_XHTML, "i", PREFIX_XHTML
        )

        val ELEM_EMPHASIZED = QName(
            NAMESPACE_XHTML, "em", PREFIX_XHTML
        )

        val ELEM_STRONG = QName(
            NAMESPACE_XHTML, "strong", PREFIX_XHTML
        )

        val ELEM_BREAKLINE = QName(
            NAMESPACE_XHTML, "br", PREFIX_XHTML
        )

        val ELEM_DIV = QName(
            NAMESPACE_XHTML, "div", PREFIX_XHTML
        )

        val ELEM_SPAN = QName(
            NAMESPACE_XHTML, "span", PREFIX_XHTML
        )

        val ELEM_P = QName(
            NAMESPACE_XHTML, "p", PREFIX_XHTML
        )

        val ELEM_A = QName(
            NAMESPACE_XHTML, "a", PREFIX_XHTML
        )

        val ELEM_CODE = QName(
            NAMESPACE_XHTML, "code", PREFIX_XHTML
        )

        val ELEM_UL = QName(
            NAMESPACE_XHTML, "ul", PREFIX_XHTML
        )

        val ELEM_OL = QName(
            NAMESPACE_XHTML, "ol", PREFIX_XHTML
        )

        val ELEM_LI = QName(
            NAMESPACE_XHTML, "li", PREFIX_XHTML
        )

        val ELEM_BLOCKQUOTE = QName(
            NAMESPACE_XHTML, "blockquote", PREFIX_XHTML
        )

        val ELEM_OBJECT = QName(
            NAMESPACE_XHTML, "object", PREFIX_XHTML
        )

        val ELEM_TABLE_HEADER = QName(
            NAMESPACE_XHTML, "th", PREFIX_XHTML
        )

        val ELEM_TABLE_ROW = QName(
            NAMESPACE_XHTML, "tr", PREFIX_XHTML
        )

        val ELEM_TABLE_BODY = QName(
            NAMESPACE_XHTML, "tbody", PREFIX_XHTML
        )

        val ELEM_TABLE_DATA = QName(
            NAMESPACE_XHTML, "td", PREFIX_XHTML
        )

        val ELEM_TABLE = QName(
            NAMESPACE_XHTML, "table", PREFIX_XHTML
        )

        val ELEM_H1 = QName(
            NAMESPACE_XHTML, "h1", PREFIX_XHTML
        )

        val ELEM_H2 = QName(
            NAMESPACE_XHTML, "h2", PREFIX_XHTML
        )

        val ELEM_H3 = QName(
            NAMESPACE_XHTML, "h3", PREFIX_XHTML
        )

        val ELEM_H4 = QName(
            NAMESPACE_XHTML, "h4", PREFIX_XHTML
        )

        val ELEM_H5 = QName(
            NAMESPACE_XHTML, "h5", PREFIX_XHTML
        )

        val ELEM_H6 = QName(
            NAMESPACE_XHTML, "h6", PREFIX_XHTML
        )

        val ELEM_SUB = QName(
            NAMESPACE_XHTML, "sub", PREFIX_XHTML
        )

        val ELEM_SUP = QName(
            NAMESPACE_XHTML, "sup", PREFIX_XHTML
        )

        val ELEM_CAPTION = QName(
            NAMESPACE_XHTML, "caption", PREFIX_XHTML
        )
    }
}