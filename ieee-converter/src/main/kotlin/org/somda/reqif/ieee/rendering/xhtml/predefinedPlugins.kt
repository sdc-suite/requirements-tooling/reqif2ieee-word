package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.somda.reqif.ieee.hook.Bookmark
import java.io.File

fun predefinedPlugins(
    document: WordprocessingMLPackage,
    bookmarks: Map<String, Bookmark>,
    inputDir: File
) = arrayOf(
    Bold(),
    Strong(),
    Italic(),
    Emphasized(),
    Div(document, bookmarks),
    RootElement(document, bookmarks),
    Paragraph(document, bookmarks),
    Span(document, bookmarks),
    Linebreak(),
    Code(),
    Anchor(document),
    UnorderedList(document, bookmarks),
    OrderedList(document, bookmarks),
    ListItem(document, bookmarks),
    Blockquote(document, bookmarks),
    Object(document, inputDir),
    Table(document),
    TableData(document, bookmarks),
    TableHeader(document, bookmarks),
    Heading1(document, bookmarks),
    Heading2(document, bookmarks),
    Heading3(document, bookmarks),
    Heading4(document, bookmarks),
    Heading5(document, bookmarks),
    Heading6(document, bookmarks),
    Subscript(),
    Superscript(),
    Caption(document, bookmarks)
).map { it.handledQName() to it }.toMap()