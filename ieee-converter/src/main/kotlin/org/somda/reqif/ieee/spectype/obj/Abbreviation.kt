package org.somda.reqif.ieee.spectype.obj

data class Abbreviation(
    val name: String,
    val description: String
)