package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.datatype.Normativity

data class Annex(
    val anticipatedLetter: Char,
    val heading: FormattedString,
    val normativity: Normativity,
    val bookmark: String
)