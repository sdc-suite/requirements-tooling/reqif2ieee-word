package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString

data class Definition(
    val term: String,
    val description: FormattedString
)