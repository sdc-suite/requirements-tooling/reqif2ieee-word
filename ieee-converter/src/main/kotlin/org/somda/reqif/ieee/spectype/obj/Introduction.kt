package org.somda.reqif.ieee.spectype.obj

data class Introduction(val heading: String)