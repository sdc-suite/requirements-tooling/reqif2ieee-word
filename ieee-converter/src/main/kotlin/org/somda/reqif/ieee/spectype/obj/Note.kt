package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.hook.Bookmark

data class Note(
    val text: FormattedString,
    val bookmark: String,
    val label: String
) {
    companion object {
        const val BOOKMARK_PREFIX = "Note:"
        const val BOOKMARK_PREFIX_SHORT = "N"

        fun expandShortBookmark(name: String): String? {
            val split = name.split(delimiters = *arrayOf(":"), limit = 2)
            if (split.size == 2 && split[0] == BOOKMARK_PREFIX_SHORT) {
                return "${BOOKMARK_PREFIX}${split[1]}"
            }
            return null
        }

        fun bookmarkFor(name: String) =
            Bookmark("${BOOKMARK_PREFIX}${Bookmark.sanitizeName(name)}", "${BOOKMARK_PREFIX}${Bookmark.sanitizeName(name)}")
    }

    fun bookmarkName() = "$BOOKMARK_PREFIX${bookmark}"
    fun shortBookmark() = "$BOOKMARK_PREFIX_SHORT:${bookmark}"
    fun bookmark() = Bookmark(bookmarkName(), label)
}