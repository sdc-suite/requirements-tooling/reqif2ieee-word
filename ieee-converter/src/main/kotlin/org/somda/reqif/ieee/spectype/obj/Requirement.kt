package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.datatype.RequirementLevel
import org.somda.reqif.ieee.datatype.RequirementNumber
import org.somda.reqif.ieee.datatype.SdcRole
import org.somda.reqif.ieee.hook.Bookmark

data class Requirement(
    val text: FormattedString,
    val number: RequirementNumber,
    val prefix: String,
    val level: RequirementLevel,
    val sdcRole: SdcRole,
    val icsFeature: String,
    val icsGroup: String,
    val icsStatus: String
) {
    companion object {
        const val BOOKMARK_PREFIX = "Requirement:"
        private const val BOOKMARK_PREFIX_SHORT = "R"

        fun expandShortBookmark(name: String): String? {
            val split = name.split(delimiters = *arrayOf(":"), limit = 2)
            if (split.size == 2 && split[0] == BOOKMARK_PREFIX_SHORT) {
                return "${BOOKMARK_PREFIX}${split[1]}"
            }
            return null
        }
    }

    fun tag() = "$prefix${tagWithoutPrefix()}"
    fun tagWithoutPrefix() = "R${zeroPaddedNumber()}"
    fun zeroPaddedNumber() = String.format("%04d", number)

    fun bookmarkName() = "$BOOKMARK_PREFIX${number}"
    fun shortBookmark() = "$BOOKMARK_PREFIX_SHORT:${number}"
    fun bookmark() = Bookmark(bookmarkName(), tag())
}