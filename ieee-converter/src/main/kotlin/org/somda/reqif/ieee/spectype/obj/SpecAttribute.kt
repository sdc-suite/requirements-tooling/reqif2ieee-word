package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.AttrType

enum class SpecAttribute(val attrName: String, val attrType: AttrType) {
    FORMATTED_STRING_VALUE("ReqIF.Text", AttrType.XHTML),
    STRING_VALUE("ReqIF.Text", AttrType.STRING),
    FORMATTED_STRING_KEY("ReqIF.ChapterName", AttrType.XHTML),
    STRING_KEY("ReqIF.ChapterName", AttrType.STRING),
    REQUIREMENT_LEVEL("RequirementLevel", AttrType.ENUM),
    REQUIREMENT_NUMBER("RequirementNumber", AttrType.INT),
    REQUIREMENT_PREFIX("RequirementPrefix", AttrType.STRING),
    SDC_ROLE("Role", AttrType.ENUM),
    LIST_TYPE("ListType", AttrType.ENUM),
    NORMATIVITY("Normativity", AttrType.ENUM),
    BOOKMARK("Bookmark", AttrType.STRING),
    CAPTION("Caption", AttrType.XHTML),
    PAGE_ORIENTATION("PageOrientation", AttrType.ENUM),
    ICS_GROUP("IcsGroup", AttrType.STRING),
    ICS_FEATURE("IcsFeature", AttrType.STRING),
    ICS_STATUS("IcsStatus", AttrType.STRING)
}