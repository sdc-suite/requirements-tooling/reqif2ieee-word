package org.somda.reqif.ieee.spectype.obj

data class VoidData(val name: String)