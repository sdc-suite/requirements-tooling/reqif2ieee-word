package org.somda.reqif.ieee.spectype.relation

data class Complements(val description: RelationDescription = RelationDescription())