package org.somda.reqif.ieee.spectype.relation

data class RelatesTo(val description: RelationDescription = RelationDescription())