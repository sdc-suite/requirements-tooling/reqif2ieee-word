package org.somda.reqif.ieee.spectype.relation

import org.somda.reqif.ieee.datatype.IeeePlainString

data class RelationDescription(
    val prefix: IeeePlainString = "",
    val suffix: IeeePlainString = ""
)