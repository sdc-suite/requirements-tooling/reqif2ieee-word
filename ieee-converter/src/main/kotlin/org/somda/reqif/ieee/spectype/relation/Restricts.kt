package org.somda.reqif.ieee.spectype.relation

data class Restricts(val description: RelationDescription = RelationDescription())